import argparse
import os
from jinja2 import Environment, PackageLoader, select_autoescape


parser = argparse.ArgumentParser()
parser.add_argument("-p", "--pipeline", help="The pipeline template file to use", required=True)
parser.add_argument("-o", "--output", help="The output file to write the pipeline to", required=True)
parser.add_argument("-l", "--layer-url", help="The location ofthe ecw file to use", required=True)
#parser.add_argument("-t", "--tiles-file", help="The location of the tiles file to use", default="/nfs/deepgeostat/deepgeostat_input/config/vierkanten_500m_selected.shp")
parser.add_argument("-t", "--tiles-file", help="The location of the tiles file to use", default="/notebooks/deepgeostat_input/config/vierkanten_500m_selected.shp")
parser.add_argument("-ts", "--tiles-size", help="The size of the tiles to use in meter", default=500)
parser.add_argument("-px", "--pixel-resolution", help="The number of pixels per meter", default=4)
#parser.add_argument("-d", "--output-directory", help="The output directory to write the tiles to", default="/nfs/deepgeostat/deepgeostat_input/tilesusb/LR/grid_500m/")
parser.add_argument("-d", "--output-directory", help="The output directory to write the tiles to", default="/notebooks/deepgeostat_input/tilesusb/LR/grid_500m/")
parser.add_argument("-y", "--year", help="The year to use for the tiles", default="2018")
parser.add_argument("-c", "--color-type", help="The color type to use for the tiles", required=True)

args = parser.parse_args()

env = Environment(
    loader=PackageLoader('create_pipeline', 'templates'),
    autoescape=select_autoescape()
)

template = env.get_template(args.pipeline)

layer_name = f"{args.year}_{args.color_type}"
with open(args.output, "w") as pipeline_file:
    pipeline_file.write(template.render(
        input_layer=args.layer_url,
        tiles_file=args.tiles_file,
        tiles_size=args.tiles_size,
        tile_width=args.pixel_resolution * int(args.tiles_size),
        tile_height=args.pixel_resolution * int(args.tiles_size),
        output_directory=os.path.join(args.output_directory, f"{layer_name}/"),
        layer_name=layer_name
    ))
