from jinja2 import Environment, PackageLoader, select_autoescape
import argparse
import os
import re


parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input-filename", help="Input filename", default="ecw_paths.txt")
parser.add_argument("-p", "--pipeline", help="The pipeline template file to use", default="ecw_nl_selected_template.json")
parser.add_argument("-o", "--output-directory", help="The output directoryfile to write the pipeline to", default="./pipelines")
args = parser.parse_args()

tile_size_metadata = [
    {
        "tiles_file": "/notebooks/deepgeostat_input/config/vierkanten_500m_selected.shp",
        "tiles_size": 500,
        "pixel_resolution": 4,
        "raster_path": "/notebooks/rasters",
        "output_directory": "/notebooks/deepgeostat_input/tilesusb/LR/grid_500m/",
    },
    {
        "tiles_file": "/notebooks/deepgeostat_input/config/vierkanten_100m_selected.shp",
        "tiles_size": 100,
        "pixel_resolution": 4,
        "raster_path": "/notebooks/rasters",
        "output_directory": "/notebooks/deepgeostat_input/tilesusb/LR/grid_100m/",
    },
]


env = Environment(
    loader=PackageLoader('create_pipelines', 'templates'),
    autoescape=select_autoescape()
)

template = env.get_template(args.pipeline)

with open(args.input_filename) as input_file:
    for layer_url in input_file.readlines():
        for tile_metadata in tile_size_metadata:
            layer_url =  os.path.abspath(os.path.join(tile_metadata["raster_path"], layer_url.strip()))
            tiles_file = tile_metadata["tiles_file"]
            tiles_size = tile_metadata["tiles_size"]
            pixel_resolution = tile_metadata["pixel_resolution"]
            output_directory = tile_metadata["output_directory"]
            print("Writing pipeline for layer: {} and size: {}".format(layer_url, tiles_size))

            color_type = "CIR" if "cir" in layer_url.lower() else "RGB"

            year_match = re.search(r"\d{4}", layer_url)
            year = layer_url[year_match.start():year_match.end()]
            layer_name = f"{year}_{color_type}"

            pipeline_filename = f"{args.output_directory}/{layer_name}_{tiles_size}m_{pixel_resolution}px.json"
            with open(pipeline_filename, "w") as pipeline_file:
                pipeline_file.write(template.render(
                    input_layer=layer_url,
                    tiles_file=tiles_file,
                    tiles_size=tiles_size,
                    year=year,
                    tile_width=pixel_resolution * int(tiles_size),
                    tile_height=pixel_resolution * int(tiles_size),
                    output_directory=os.path.join(output_directory, f"{layer_name}/"),
                    layer_name=layer_name
                ))
