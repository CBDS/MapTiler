# MapTile

MapTiler is a program that helps downloading and preprocessing tiles from a
geoservice or geoimage format, as for example WMS, WMTS, or GeoTiff. To process
the tiles, MapTiler implements a processing pipeline in which tiles are first
downloaded, then preprocessed on the GPU, before written to disk. MapTiler is
implemented in c++ and uses libgdal to provide read and write support for a wide
variety of geodata formats.

# Building MapTiler

Currently MapTiler can be build in three ways:

* A local build on a Ubuntu Linux machine
* Using a dockerfile (Dockerfile) to build MapTiler *without* ECW support
* Using another dockerfile (ecw.Dockerfiler) to build MapTiler *with* ECW
  support

We will describe each of these options in the subsections below.

## Build MapTiler on a Ubuntu Linux machine

MapTiler has a number of dependencies that have to be installed before
attempting to build the software.

* **CMake** (>= 3.5),
* **libgdal** (>= 2.2),
* **Qt5** (>= 5.10), in specific the QLocation and QCoreApplication classes,
* **libpthread**
* **libpng-dev**
* **uuid-dev**
* **libGL**, **libGLEW**, **libGLU** and **libglfw**

In addition to that, MapTiler is implemented using the C++14 standard. To be
compiled, it needs a compiler that support this standard. Any modern gcc or
llvm/clang compiler should be sufficient. It has been successfully compiled
using GCC 5.0 and GCC 7.0.

### Ubuntu Installation

To install the above dependencies in ubuntu type the following:

~~~~console
sudo apt-get install cmake libpng-dev uuid-dev libgl-dev libglew-dev
libglu-dev libglfw3-dev libgdal-dev
~~~~

Also to install qt 5 download the latest (open-source) qt 5 package (>= 5.10)
at:

[Qt5 Download](https://www.qt.io/download)

Follow the instructions there to install it. Make sure you install Qt under
$HOME in the $HOME/Qt directory. For MapTiler to work out of the box install Qt
5.10.1 next to the current version. If you get an error about some SSL libs not
being able to load, run the following command:

sudo apt install libssl1.0-dev

Alternatively you can add the following ppa to your Ubuntu apt to download the
Qt 5 binaries from there [Qt5 ppa for Ubuntu](https://launchpad.net/~beineri).
The whole procedure can be executed as follows:

~~~~console
sudo apt install -y software-properties-common
sudo add-apt-repository ppa:beineri/opt-qt-5.14.1-bionic
sudo apt update
sudo apt install qt514-meta-full
~~~~

This add the apt repository, downloads and installs Qt 5.14.

### Compilation

MapTiler can be compiled using CMake. In the location that you would like to
save and build the code run the following in the terminal:

~~~~console
git clone https://github.com/thinkpractice/MapTiler.git
cd MapTiler cmake .
make
~~~~

# Building MapTiler Docker without ECW support

To build MapTiler *without* Erdas ECW support you can use the provide
Dockerfile. All dependencies are included in the docker file so it is sufficient
to build it as follows:

~~~~console
docker build . -f Dockerfile -t maptiler
~~~~

Here we build the Dockerfile and give it the tag "maptiler" for future
reference.

# Building MapTiler Docker with ECW support

To build MapTiler *with* Erdas ECW support you can use the ecw.Dockerfile.
Before building the dockerfile the Hexagon Erdas ECW driver version 5.4 needs to
be downloaded and copied to the erdas folder relative to the repository root.
The Erdas ECW driver that needs to be installed is the Linux version and can be
found here: [Hexagon Erdas ECW 5.4 Driver
Download](https://download.hexagongeospatial.com/downloads/ecw/erdas-ecw-jp2-sdk-v5-4-linux).

After downloading the Erdas Drivers and unzipping the archive, you will need to
make the ERDAS_ECWJP2_SDK-5.4.0.bin executable and run it like this:

~~~~console 
chmod +x ./ERDAS_ECWJP2_SDK-5.4.0.bin 
./ERDAS_ECWJP2_SDK-5.4.0.bin
~~~~
 
To retrieve the folders with the ECW driver, first a choice has to be made
between several licence options. The docker uses the read-only driver that is
free. To continue enter 1 for "Desktop Read-Only Redistributable" and accept the
licence conditions in the next screen. When ready, a folder with the name
hexagon will have been unpacked in the same folder that contains the
ERDAS_ECWJP2_SDK-5.4.0.bin script. In this folder, there is a
ERDAS-ECW_JPEG_2000_SDK-5.4.0 subfolder. Copy or move this folder to the erdas
folder relative to the repository root. Now the docker file can be build with
the following command:


~~~~console 
docker build . -f ecw.Dockerfile -t maptiler_ecw 
~~~~

Here we build the Dockerfile and give it the tag "maptiler_ecw" for future
reference.

# Running MapTiler

As we saw above, MapTiler can be build and installed locally or within a docker
container. Likewise, MapTiler can be run locally or from within the docker. In
both cases for MapTiler to tile a certain map we need a json file that defines a
pipeline with the steps that have to applied to the map. For more information
see the section about The Pipeline settings file below.

## Running MapTiler locally

The general syntax to run MapTiler locally looks like this:

~~~~console 
./MapTiler -p pipeline_file.json 
~~~~

## Running MapTiler from a Docker

To run MapTiler from within the Docker we need to also bind some volumes that
MapTiler will use to read and store the data from within the Docker container.
For MapTiler to run correctly we need three to bind three volumes:

1. the **config** volume, which contains the pipeline files, we can bind this
   volume to the [example_pipelines](../example_pipelines) directory in the
repository root for example.
2. the **maps** volume, this is the *source volume* that contains the map files
   to read from. We can bind this volume to a directory that contains certain
raster files, for example geotiffs or ecw files, that we would like MapTiler to
read.
3. the **tiles** volume, this is the *destination volume* to which MapTiler
   writes the created tiles. We should set this volume to the directory that
should contain the created tiles in the end.

The docker call including the bound volumes would then typically look like this:

~~~~console 
docker run -v /full/path/to/example_pipelines:/config -v
/full/path/to/map_images:/maps -v
/full/path/to/where/tiles/should/be/stored:/tiles -it maptiler_ecw
/config/docker_ecw_example.json 
~~~~

If instead the normal version of the docker should be used the *maptiler_ecw*
tag above (right after -it) should be replaced with *maptiler*. Also an
appropriate pipeline json file should be selected.

### Options

The following options can be provided to the application:

* **-h**, **--help**, displays a help message.
* **-v**, **--version**, displays version information.
* **-p**, **--pipeline_settings**, a json file containing the settings for a
  MapTiler processing pipeline.
* **--address** *location*, the *location* (address/city name/region) for which
  the tiles should be downloaded.
* **-a**, **--addressoption** *locationoption*, the location option to choose if
  the address gives back multiple option (default=first).
* **-t**, **--target-directory** *directory*, copy all the tiles into
  *directory*.

### Arguments

Unless the a polygon file has been specified MapTiler also needs the following
argument:

* **rasterurl**, *url* to raster webservice (WMS/WMTS) with the aerial image.

# Pipeline settings file

To process map files and create tiles out of them, MapTiler uses pipelines.
Pipelines consist of several processing steps and can be configured using json
setting files. Currently, a pipeline can consist of the following processing
steps:

* **AddMetadataStep**, to add extra metadata from another information source to
  each tiles. For example, the addresses present in each tile can be added from
a register/table containing geoinformation about addresses.
* **GridProducerStep**, a step that creates the tiles from a regular grid for
  which the bounding box was specified.
* **TileFromLocationProducerStep**, a step that creates the tiles from a layer
  containing geolocations.
* **TileFilterStep**, a step that can filter tiles according to some metadata
  source added earlier by AddMetadataStep. This step can be used to for example
filter out all tiles without addresses.
* **TileDownloadStep**, downloads the tile from a map file or webservice.
* **TileGpuTransferStep**, masks the tiles with information from a certain
  metadata source added earlier by AddMetadataStep. For example, a data source
containing the building polygons can be used to cutout the buildings from a
tile.
* **TileWriterStep**, writes the tiles to disk in a specified format (jpeg,
  tiff, etc.)

