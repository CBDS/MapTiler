#include "ProcessingStep.h"

PipelineStep::PipelineStep(StepType type)
                    :   _type(type)
{
}


PipelineStep::~PipelineStep()
{
}

PipelineStep::StepType PipelineStep::Type()
{
    return _type;
}

ProcessingStep::ProcessingStep()
                    :	PipelineStep(Processing)
{
}

ProcessingStep::~ProcessingStep()
{
}

ProcessingStep::ProcessingStep(StepType type)
                    :   PipelineStep(type)
{
}

vector<ProcessingStep::PipelineStepType> ProcessingStep::NextSteps()
{
    return _nextSteps;
}

void ProcessingStep::AddNextStep(PipelineStepType nextStep)
{
    _nextSteps.push_back(nextStep);
}

void ProcessingStep::MoveToNextSteps(ProcessingStep::StepDataType stepData)
{
    for (auto nextStep : _nextSteps)
    {
        nextStep->Process(stepData);
    }
}

SourceStep::SourceStep()
                : ProcessingStep(Source)
{
}

SourceStep::~SourceStep()
{
}

SinkStep::SinkStep()
            :	PipelineStep(Sink)
{
}

SinkStep::~SinkStep()
{
}


