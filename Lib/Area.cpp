#include "Area.h"
#include <iostream>
#include <iomanip>

Area::Area()
        : Area(0.0, 0.0, 0.0, 0.0)
{
}

Area::Area(const SpatialReference& projectionReference, const Point& leftTop, const Point& bottomRight, string description)
        :   _spatialReference(projectionReference),
            _leftTop(leftTop),
            _bottomRight(bottomRight),
            _description(description)
{
}

Area::Area(const SpatialReference& projectionReference,
            const Point& leftTop,
            const Point& bottomRight)
        :   Area(projectionReference, leftTop, bottomRight, "")
{
}

Area::Area(double minX, double minY, double maxX, double maxY, int epsgCode, string description)
        :   Area(SpatialReference::FromEPSG(epsgCode), Point(minX, minY), Point(maxX, maxY), description)
{
}

Area::Area(const shared_ptr<Geometry> geometry)
{
   SetAreaFromGeometry(geometry);
}

SpatialReference Area::ProjectionReference() const
{
    return _spatialReference;
}

void Area::SetProjectionReference(const SpatialReference &projectionReference)
{
    _spatialReference = projectionReference;
}

void Area::SetEPSG(int epsg)
{
    SetProjectionReference(SpatialReference::FromEPSG(epsg));
}

Point Area::LeftTop() const
{
    return _leftTop;
}

void Area::SetLeftTop(const Point &leftTop)
{
    _leftTop = leftTop;
}

Point Area::BottomRight() const
{
    return _bottomRight;
}

void Area::SetBottomRight(const Point &bottomRight)
{
    _bottomRight = bottomRight;
}

Point Area::TopRight() const
{
    return Point(_bottomRight.X, _leftTop.Y);
}

Point Area::BottomLeft() const
{
    return Point(_leftTop.X, _bottomRight.Y);
}

std::string Area::RasterId(const unsigned int numberOfDigits) const
{
    std::ostringstream  rasterId;
    std::string x_coord = std::to_string(BottomLeft().X);
    std::string y_coord = std::to_string(BottomLeft().Y);

    //fill x_coord up to 6 positions by adding padding zeroes in the front
    size_t decimalIndex = x_coord.find(".");
    std::ostringstream x_coord_filled;
    x_coord_filled << std::setfill('0') << std::setw(6) << x_coord.substr(0, decimalIndex) ;

    rasterId << "E" << x_coord_filled.str().substr(0, numberOfDigits)  << "N" << y_coord.substr(0, numberOfDigits);
    return rasterId.str();
}

void Area::SetDescription(string description)
{
    _description = description;
}

string Area::Description() const
{
    return _description;
}

void Area::SetAreaFromGeometry(const std::shared_ptr<Geometry> geometry)
{
    SetProjectionReference(geometry->GetSpatialReference());
    if (geometry->IsPolygon())
    {
        auto polygon = dynamic_pointer_cast<Polygon>(geometry);
        Rect boundingBox = polygon->BoundingBox();

        //Y-direction is mirrored here, a latitude starts at the equator,
        //while a computer screen starts at the top left.
        SetLeftTop(boundingBox.BottomLeft());
        SetBottomRight(boundingBox.RightTop());
    }
}
