#include "TileFilterStep.h"
#include "Rect.h"
#include "StepData.h"

TileFilterStep::TileFilterStep(std::string layerName)
                    :	ProcessingStep(Processing),
                        _layerName(layerName)
{
}

TileFilterStep::~TileFilterStep()
{
}

void TileFilterStep::Process(StepDataType stepData)
{
    vector<Feature> features = stepData->GetMetadataFeatures(_layerName);
    if (features.size() > 0)
    {
        MoveToNextSteps(stepData);
    }
    else
    {
        StepData::SetNumberOfTiles(StepData::NumberOfTiles() - 1);
    }
}
