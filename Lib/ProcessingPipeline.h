#ifndef PROCESSINGPIPELINE_H
#define PROCESSINGPIPELINE_H

#include <vector>
#include <memory>
#include <thread>
#include "ProcessingStep.h"
#include "SafeQueue.h"
#include "GeoTile.h"

class ProcessingPipeline
{

public:
    ProcessingPipeline();
    virtual ~ProcessingPipeline();

    using StepPtrType = std::shared_ptr<PipelineStep>;

    void StartProcessing();
    void AddPipelineStep(StepPtrType step);

private:
    std::vector<StepPtrType> _pipelineSteps;
    std::vector< std::thread > _threads;
};

#endif /* PROCESSINGPIPELINE_H */
