#include "ProcessingPipeline.h"

ProcessingPipeline::ProcessingPipeline()
{
}

ProcessingPipeline::~ProcessingPipeline()
{
    /*for (auto& thread : _threads)
        thread.join();*/
}

void ProcessingPipeline::StartProcessing()
{
   if (_pipelineSteps.size() == 0)
        return;

   auto firstStep = _pipelineSteps[0];
   if (firstStep->Type() == PipelineStep::Source)
   {
        shared_ptr<SourceStep> sourceStep = static_pointer_cast<SourceStep>(firstStep);
        sourceStep->Run();
   }

   /* for (auto& step : _pipelineSteps)
        _threads.push_back(thread([&]
                    {
                        step->Run();
                    })
                );*/
}

void ProcessingPipeline::AddPipelineStep(std::shared_ptr<PipelineStep> step)
{
    int previousStepIndex = static_cast<int>(_pipelineSteps.size() - 1);
    cout << "In AddPipelineStep" << endl;
    if (step->Type() != PipelineStep::Source && previousStepIndex > -1)
    {
        auto previousStep = _pipelineSteps[previousStepIndex];
        cout << "Add Next Step" << endl;
        if (previousStep->Type() != PipelineStep::Sink)
        {
            shared_ptr<ProcessingStep> previousProcessingStep = static_pointer_cast<ProcessingStep>(previousStep);
            previousProcessingStep->AddNextStep(step);
        }
    }
    _pipelineSteps.push_back(step);
}
