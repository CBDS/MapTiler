#include "TileDownloadStep.h"
#include "StepData.h"
#include "GeoMapProvider.h"
#include "Utils.h"
#include <chrono>
#include <thread>

TileDownloadStep::TileDownloadStep(std::string layerName, std::string layerUrl, int layerIndex, int year, std::vector<std::string> driverOptions)
                    :	TileDownloadStep(layerName, Utils::LoadRasterMap(layerUrl, layerIndex, driverOptions), year)
{
}

//TODO remove later
TileDownloadStep::TileDownloadStep(std::string tileName, std::shared_ptr<GeoMap> map, int year)
                    :	ProcessingStep(),
						_tileName(tileName),
                        _map(map),
                        _year(year)
{
}

TileDownloadStep::~TileDownloadStep()
{
}

void TileDownloadStep::Process(StepDataType stepData)
{
	int numberOfTilesDownloaded = 0;

    try
    {
        std::shared_ptr<GeoTile> tile = _map->GetTileForRect(stepData->BoundingRect());
        stepData->AddTile(_tileName, tile, _year);

        MoveToNextSteps(stepData);
        numberOfTilesDownloaded++;
        //cout << "raster 4: " << stepData->BoundingArea().RasterId() << " raster 6: " << stepData->BoundingArea().RasterId(6) << endl;

        if (numberOfTilesDownloaded % 100 == 0)
        cout << "Downloaded " << to_string(numberOfTilesDownloaded) << " out of " << to_string(StepData::NumberOfTiles()) << endl;

        /*if (numberOfTilesDownloaded > 0 && numberOfTilesDownloaded % 100 == 0)
            {
                cout << "Download of " << _tileName << " is sleeping for a while" << endl;
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        }*/
    }
    catch (...)
    {
    }

}
