#ifndef PROCESSINGSTEP_H
#define PROCESSINGSTEP_H

#include <memory>
#include "Rect.h"
#include "StepData.h"
#include "SafeQueue.h"

using namespace std;

class PipelineStep
{
    public:
        enum StepType { Source, Processing, Sink };

        using StepDataType = shared_ptr<StepData>;

    public:
        PipelineStep(StepType type);
        virtual ~PipelineStep();

        StepType Type();
        virtual void Process(StepDataType stepData) = 0;

    private:
        StepType _type;
};

class ProcessingStep : public PipelineStep
{
public:
    using PipelineStepType = shared_ptr<PipelineStep>;

public:
    ProcessingStep();
    virtual ~ProcessingStep();

protected:
    ProcessingStep(StepType type);

public:
    vector<PipelineStepType> NextSteps();
    void AddNextStep(PipelineStepType nextStep);
    void MoveToNextSteps(StepDataType stepData);

protected:
    vector<PipelineStepType> _nextSteps;
};


class SourceStep : public ProcessingStep
{
public:
    SourceStep();
    virtual ~SourceStep();

    virtual void Run() = 0;
};

class SinkStep : public PipelineStep
{
public:
    SinkStep();
    virtual ~SinkStep();

};

#endif /* PROCESSINGSTEP_H */
