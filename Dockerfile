FROM ubuntu:18.04
RUN apt update && \
    apt install -y software-properties-common && \
    add-apt-repository ppa:beineri/opt-qt-5.14.1-bionic && \
    apt update && \
    apt upgrade -y && \
    apt install -y \
        cmake \
        libpng-dev \
        uuid \
        libgl-dev \
        libglew-dev \
        libglu-dev \
        libglfw3-dev \
        libgdal-dev \
        qt514-meta-full \
        git

WORKDIR /docker_build

COPY . /docker_build/MapTiler

RUN mkdir /docker_build/MapTiler/docker_build && \
    cd /docker_build/MapTiler/docker_build && \
    rm -f /docker_build/MapTiler/CMakeCache.txt && \
    cmake ..
RUN cd /docker_build/MapTiler/docker_build && make clean && make

VOLUME /maps
VOLUME /tiles
VOLUME /config

WORKDIR /config

ENTRYPOINT ["/docker_build/MapTiler/docker_build/MapTiler", "-p"]
